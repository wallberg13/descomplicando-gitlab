# Day 05 - Gerando Builds de Imagens de Container

O objetivo da aula do presente dia, é criar uma imagem docker (com o Dockerfile) e subir em uma máquina EC2 via Terrafrom. Essa mesma imagem estará disponível em um register do docker.

kaniko.
Não podemos realizar build de imagem docker dentro de um ambiente de container. Pois realizar build requer acesso privilegiado, o que não é ideal.

CI_REGISTRY: endereço do nosso register

## Aula 01 - O que foi feito?

- Foi basicamente, criado uma CI com execução manual, onde o mesmo pega
  um Dockerfile, gera a build do mesmo e coloca dentro de um register do projeto. Para isso, foi utilizado o kaniko para realizar builds no qual a mesma não é executada em modo usuário.
  Basicamente, eu entendi que ele jogou alguma coisa dentro de alguma pasta (do kaniko), e fez o build do dockerfile e subiu a imagem para o registry do gitlab.

## Aula 02 - Criando o Terraform para jogar no EC2.

- Basicamente, agora vamos criar uma infraestrutura na AWS com o Terraform.
  Inicialmente, iremos criar o variables e provavelmente já vamos
  precisar passar algumas credenciais para o TF para que possamos subir nossa infraestrutura.
- Então, o que foi feito:
  - Foi criado um IAM no AWS, que é um usuário que pode acessar minha conta e o mesmo possui acesso à uma chave de api e de acesso. Criado na AWS, o Gitlab já tem uma variável padrão, só esperando para ser criada com essas credenciais.
  - No Terraform, basicamente, foi criado algumas variáveis (bem basicas) e foi chamado o provider da aws de forma bem simploria. E no arquivo principal (main.tf), foi criado um recurso, preenchendo o valor das variáveis.
  - No arquivo CI, foi feito a mesma coisa que foi feito no terraform do projeto do datadog. A diferença é que foi utilizado um `artifacts` ao executar o plan do terraform, com a intenção de "exportar" um arquivo gerado para outro lugar, e o gitlab é quem faz isso (mas ainda sim, não adiantou de nada pois o container não foi destruido.)
  - Foi preciso realizar um passo a mais, que era o de adicionar uma chave publica (mas na vdd, era só verificar se tinha algum par de chaves criado, que daria certo).

## Dica de GitLab Variables

- Protected variables só são executadas em branchs protegidas ou tageadas.
