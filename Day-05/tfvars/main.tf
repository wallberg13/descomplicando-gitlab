resource "aws_key_pair" "meu_par_chaves" {
  key_name = var.ec2_key_name
  public_key = var.PUBLIC_KEY
}
resource "aws_instance" "creating_ec2" {
  ami = var.ec2_image_id
  instance_type = var.ec2_instance_type
  key_name = aws_key_pair.meu_par_chaves.key_name
  count = var.ec2_instance_count # Aqui indico que quero mais de uma máquina
  tags = {
    Name = var.ec2_tags
  }
}

# Então na saida do pipeline, como eu posso ter mais de uma máquina
# preciso colocar uma regex para exibir todas as saidas
output "instance_ip" {
  # * para ser saída de todas as intancias
  value = aws_instance.creating_ec2.*.public_ip
}

output "instance_private_ip" {
  # * para ser saída de todas as intancias
  value = aws_instance.creating_ec2.*.private_ip
}
