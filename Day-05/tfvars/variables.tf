variable "ec2_region" {
  default = "us-east-1"
}

variable "ec2_key_name" {
  default = "descomplicando_gitlab"
}

variable "ec2_instance_type" {
  default = "t2.micro"
}

variable "ec2_image_id" {
  default = "ami-04505e74c0741db8d"
}

variable "ec2_tags" {
  default = "descomplicando o gitlab"
}

variable "ec2_instance_count" {
  default = 1
}

variable "PUBLIC_KEY" {
  type = string
}