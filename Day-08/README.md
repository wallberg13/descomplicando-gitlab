# Day 08 - Subindo o Gitlab em um Cluster EKS CTL

EKS (Elastic Kurbenetes Cluster) serve para gereciar um cluster Kurbenetes da Nuvem da AWS. Basicamente, ele faz
deploy de servidores.

Basicamente, é tudo voltado para AWS, então precisamos dispor de uma qunatia em dinheiro
e de servidores (máquinas um pouco parrudas para subir o serviço.). Lembrando que na
nossa máquina, precisamos ter um arquivo de config e credentials.

Na AWS:

- No IAM:
  - Realizar a criação de um usuário (com as permissões corretas). AWS Credentials.

[Instalando o EKS na máquina](https://eksctl.io/introduction/)

```bash
# Criando um Cluster
eksctl create cluster -f <nome_arquivo.yaml>
```

# DNS-External -> Colocando um dominio no ClusterKuber (Route 53 - serviço de DNS / Dominios).

Para apontar um DNS para o Cluster, foi utilizado o serviço Route 53 da AWS, no qual o mesmo faz
todo o gerenciamento de domínios.

Criando um sub dominio:

```bash
aws route53 create-hosted-zone --name "eks.strigus.com.br." --caller-reference "external-dns-test-$(date +%s)"

# Criando um IAMServiceAccount
eksctl create iamserviceaccount --name external-dns --namespace default --cluster <Cluster Name> --attach-policy-arn <policy arn> --approve # Dando erro, é só executar o comnado que o eks cospe e já era.
```

- Cria policy
- Cria role
- Cria IAM ServiceAccount
- Realiza o deploy do External-DNS (o kra cospe uma arquivo do nada).

# Certmanager via Helm (Package Manager for Kubernetes).

[Link para Download](https://cert-manager.io/docs/installation/helm/).
