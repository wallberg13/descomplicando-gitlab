# Day 04 - Projeto do Curso - Pipelines

O projeto vai envolver criar um monitor no Datadog utilizando Terraform.
Terraform é uma ferramenta para definir infraestrutura como código. Mas ainda não sei onde
e nem como aplicar ele. Mas é muito utilizado por DevOps e não sabia que para fazer este curso iria
precisar. Mas bola para frente. rsrsrs.

Então até agora, o que precisei:

- Criar uma conta do Datadog (em `app.datadoghq.com`).
- Criar uma máquina virtual linux no meu PC (utilizando o `multipass`).
- Instalar um agente do `datadog`, conforme solicitado pelo site do Datadog.
- Procurar saber como que funciona o Terraform (em relação ao Datadog) na sua wiki em [DataDog](https://registry.terraform.io/providers/DataDog/datadog/latest/docs)
- Criar os arquivos: `providers.tf` (com a instrução do provider do `Datadog`) e com as variáveis necessárias. `variables.tf` com a declaração das variáveis e tipos que serão utilizados. E o `main.tf` com o que realmente vai acontecer.
- Foi instalado o `terraform` com o `Brew`, via tutorial no próprio site do Terraform.
- Agora, dentro da pasta do `datadog`, será dado um `terraform init`.
- Com o terraform iniciado, iremos fazer um `terraform plan`. Na saída do comando,
  ele diz o que será feito, e como será feito.
- Dando um `terraform apply`. Ele irá aplicar tudo aquilo que ele disse que aplicaria na saída do comando anterior.
- Para remover / destruir o nosso monitor, foi preciso apenas a execução do comando: `terraform destroy`.

Tirando uma breve conclusão, o _Terraform_ é um cara que executa scripts em cima de infraestruturas.
E o mesmo precisa ter as integrações com a AWS da vida, para que o mesmo possa montar a infra do
jeito que precisamos. Então basicamente, o que existe, é um provider da aws, fornecendo uma API para
criar e excluir máquinas na AWS (em caso de integração com AWS).

# O que é um Pipeline ([Doc](https://docs.gitlab.com/ee/ci/yaml/))?

É uma sequência de comandos que é executado pelo o Gitlab, ou um Git Actions ou qualquer coisa da vida.

Uma coisa que sempre temos que ter em mente, é que um _pipeline_ é sempre executado dentro de um _container_.
Então para tal, iremos precisar selecionar alguma imagem docker para poder executar essa bagaça.

Para esse projeto nosso que envolve o Terraform. Iremos utilizar a [imagem do terraform](https://gitlab.com/gitlab-org/terraform-images)

Formato de um arquivo .yml

```yml
image: <nome da imagem> # É a imagem Docker que iremos utilizar para executar o pipeline. Ouuuu

image:
  name: <nome_da_imagem>
  entrypoint: ["/bin/bash", "-c"] # O que será executado quando iniciar a imagem (sempre vamos querer nosso shell)

stages: # É os stagios / 'botões' disponíveis para execução do pipeline.
  - <stg_01>
  - <stg_02>
  - <stg_03>

<nome_staging / task>:  # Definindo uma Task.
  stage: <nome_stage>   # Essa task será executada no stage X
  image: <nome_image>   # Essa task será executada dentro de uma imagem X
  script:               # O que será executado (um shell da vida)
    - cd xyz/
    - exec cmd
  when: always | manual # Quando que iremos executar. Always: em todos os commits. Manual: sempre manualmente.
  only:                 # Definindo em qual branch que iremos pode executar esse Pipeline. Se não tiver na branch,
    - main              # esse pipeline nem aparece no Gitlab.
```

Uma vez criado o Pipeline, o Gitlab já executa automaticamente o mesmo. Entretanto, neste primeiro momento,
o mesmo apresenta erro, pois nós não passamos as variáveis para o **terraform** executar. Ou seja, não passamos
as credenciais.

Inicialmente, para criarmos variáveis que possam ser conhecidas pelo o GitLab (e serem reconhecidas nos CI/CD que definirmos),
iremos precisar definir as variáveis via menu lateral no GitLab, em `Settings/CI\/CD/Variables`. Dentro dessa opção, iremos criar
as variáveis necessárias para utilizarmos no nosso projeto.

No caso do _Terraform_, nós definimos no GitLab, o padrão `TF_VAR_<NOME_VARIÁVEL>`. E no _terraform_, adicionamos as variáveis,
justamente sem esse prefixo. Isso deu certo, pois o _Terraform_, ele já sabe, que o prefixo `TF_VAR_` é uma variável, e ele
já fez a atribuição do que foi definido no GitLab para as variáveis que foram definidas no _Terraform_.

No caso do _Terraform_, o mesmo ele guarda um arquivo chamado `terraform.tfstate`, onde diz tudo o que foi
executado. Entretanto, como nós estamos executando o nosso _Pipeline_ dentro de um _container_, a informação de
que um dia esse _Terraform_ já foi executado (já foi aplicado) não existe, e portanto, não é removido nada no
mesmo, pois ele acha que existe nada. Para resolver isso, podemos utilizar o _Terraform Backend State_, nova função do GitLab.
