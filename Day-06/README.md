# Day 06 - Melhorando o nosso respositório Git

### Milestone

Milestones são metas que colocamos no nosso projeto com data de inicio e fim, essa é a sua criação.
Dentro de um Milestone, podemos realizar o vinculo de vários issues, que no caso, são atividades
que podemos desenvolver para manter um projeto.

- Com um equipe engajada, pode-se utilizar isso como meta para entrega de features
  dentro de um projeto Git. É bem legal utilizarmos para isso.

QuickActions serve para gerar gatilhos dentro de um projeto GitLab.

#### Dicas de QuickActions

- [Dicas](https://docs.gitlab.com/ee/user/project/quick_actions.html)

#### Dicas de Markdown

- [Dicas](https://docs.gitlab.com/ee/user/project/quick_actions.html)

### Service Desk

- Service Desk, é uma forma de abrir "protocolos" dentro de um projeto via envio de
  email. Ao fazer um envio de email para tal endereco que foi fornecido, ele
  automaticamente abre uma issue cujo o description é o corpo do email.

### Requirements - O que o produto precisa ter?

- Requirements, são requisitos do projeto. Então basicamente, precisamos dizer o que o
  projeto precisa ter.

## O GitLab Runner

- O Gitlab Runner, é uma instancia que faz com que os actions do CI/CD possam ser executados
  em algum lugar do mundo (um runner).
- Então, basicamente, um runner é um agente no qual ele fica instalado no servidor. E
  podemos utilizar o GitLab para rodar nossos projetos com um runner dedicado e
  sem precisar compartilhar processamento com outros runners (além de não contar na
  quota imposta pelo o GitLab.).
- O Runner é o cara que executa o nosso Pipeline.

#### Instalando o Runner

- Para instalar o Gitlab Runner, só precisamos [acessar este tutorial](https://docs.gitlab.com/runner/install/), selecionar o sistema operacional que temos e selecionar o runner.

- Instalando em [containers](https://docs.gitlab.com/runner/install/docker.html).

Rodando o Container com o Docker:

- Obtendo um help: `docker run --rm -t -i gitlab/gitlab-runner --help`

  - `--rm`: container é removido após execução do container;
  - `-i -t`: modo interativo;
  - `--help`: o que está sendo executado.
  - OBS: não é legal ficar criando volumes localmente onde eu precise criar Binds (só é bom
    quando precisamos sincronizar arquivos.).

- Instalação:

```bash
docker volume create gitlab-runner-config # Criando um volume, gerenciado pelo o proprio Docker.
docker volume ls # Visualizando todos os volumes criados.
# Executando como Deamon
docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest
```

#### Config do GitRunner

Inicialmente, ou acessamos a pasta de volume (no linux), ou entro dentro do container
e vou em `/etc/gitlab-runner`, e abro o arquivo `config.toml`. E o mesmo possui o seguinte
formato:

```toml
concurrent = 1      # Quantos pipelines podemos rodar simutâneo (0 = a máquina é o limite)
check_interval = 0  # Check em todo momento (pulling)

[session_server]          # Sessão
  session_timeout = 1800  # Timeout -> O kra morreu..
```

#### Realizando o registry do GitRunner

```bash
gitlab-runner register # Realizando o registry do Gitlab Runner. Seguimos os steps.

```

- O que é o executor: como que essa parada vai ser executada?
- Podemos escolher o docker. E ele pede uma imagem default.

#### Insights

- Para o Gitlab Runner Servir com Containers, a gente precisa ter volumes na máquina, para dizer onde ele vai agir, ou que scripts ele vai poder rodar.

# Instalando o gitlab-runners no Raspberry Pi

Mesma coisa, mas indo no site `https://docs.gitlab.com/runner/install/linux-manually.html` onde tem o
tutorial para Linux e ARM64. Ai só instalar e rodar o comando para registrar.

# Colocando o Pipeline para rodar em um runner na unha.

Primeiro:

- Precisamos definir tags nos runners que estamos criando.
- Depois de definir as tags, precisamos ir no arquivo .yml para poder vincular o step do pipeline
  com uma tag, e assim, o pipeline vai ser executado no runner que tem aquela tag que acabamos adicionando
  no step.

Para o Gitlab Runner, para shell, ele precisa de algumas permissões,
para algumas coisas. Então, é sempre bom deixar as permissões de execução tudo direitinho já dentro do Nó que irá executar.
