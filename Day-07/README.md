# Day 07 - Mais Pipeline na bagaça - Avançado!!

# Services

São serviços extras que podemos "invocar" enquanto estamos testando o nosso pipeline.
No caso, tais serviços, estamos utilizando alguns serviços de banco de dados, e os mesmos
podem servir para realizar testes de integração das nossas aplicações quando o
script envolve realizar updates em banco de dados (ai ele testa se está tudo ok.).

> OBS: é uma boa pratica, deixar os services somente dentro de onde vamos utilizar os mesmos,
> pois assim, toda vez que estamos rodando uma etapa do pipeline, os mesmos estão sendo invocados
> e sem nenhuma necessidade de invocação.

# Utilizando Anchors do YAML em nosso Pipeline

Ancoras possui como objetivo, realizar reutilização de trechos de código dentro de um arquivo YAML. Ou seja, criamos um template para que possa ser reaproveitado em todo o arquivo do YAML.

No arquivo YAML, quando é definido um "step" com um `.` na frente, esse trecho irá servir
de template para outras partes do arquivo. Definindo a Ancora como um step. [Para mais informações](https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#yaml-anchors-for-scripts)

```yaml
# Definindo a ancora
.<step_name_template>: &anchor_name
  pass1:
    - option01
    - option02
    - ....
# Utilizando as ancoras
step01:
  <<:
    *anchor_name # << operador para realizar o merge do template definido lá em cima.
    # esse operador vai pegar tudo o que está na ancora e baixo dele e "jogar" neste lugar definido.
  - *anchor_name # Somente para copiar o conteudo abaixo de onde foi definido a ancora, para copiar o mesmo nível.
```

# Utilizando Templates Customizados

Templates são arquivos / trecho de código que podemos utilizar dentro dos nossos arquivos
que possuem os scripts de pipeline.

Então basicamente, estou pegando um trecho do arquivo que tinha (principal), que é a etapa de realizar build do meu container. E jogando isso para um template (vai que quero fazer mais de uma build do meu container, ainda não sei). E depois disso, provavelmente iremos realizar uma reutilização desse template.

Fluxo do template:

1. Crio o meu arquivo secundário com os steps que quero executar (se eu quiser que esse processo fique oculto, então é só add um `.` na frente do job. O `.` não faz com que o step seja executado.). Ficando assim:
   ```yaml
   < . (para deixar o step oculto) | ><nome_step>: # O Job fica escondido, ele não é executado.
     stage: xxx
     image:
       name: yyyy
       entrypoint: [...]
     script:
       - step1
       - step2
       - step3
     tags: # Selecionado Runner que irá executar
       - x
       - y
       - z
   ```
2. Dentro do nosso pipeline principal (que é o que o Gitlab executa de verdade), só fazemos um include:

   ```yaml
   include: /PATH/TO/THE/FILE # Lembrando que o RAIZ "/" é sempre a pasta do projeto, e não o root da máquina.
   ```

   > Lembrando que, a forma que fizemos o `include`, está em sua forma local. Mas podemos fazer `include` de links
   > externos ao nosso projeto. Com a opção de remote. Assim, ficaria:
   >
   > ```yaml
   > include:
   >   - remote: <link remoto>
   >   - local: <link de arquivo local>
   >   - template: <template oficial do Gitlab>
   > ```

# Extends

Extends é outra forma de reutilizar arquivos de configução, e é uma forma alternativa para o uso de ancoras. Então basicamente, definimos tudo dentro de um arquivo que podemos utilizar para templates
e depois, só chamar um extends (como se a gente tivesse puxando aquele trecho para o nosso step atual). Então basicamente, com o arquivo já dentro de um `include`, é só chamar o que está definido dentro desse `include` como se tivesse invocando uma variável qualquer.
