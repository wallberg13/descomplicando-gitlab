# Day 02 - Se aprofundando no Git.

# 01 - Chamando uma Rake Task - Reset de Senha - Modo 01 de Resetar Senha / Criar uma nova

- Inicialmente, para chamar a Rake Task, precisamos colocar no bash:

```bash
gitlab-rake "gitlab:password:reset"
```

Com isso, chama as rake task do gitlab para fazer o reset de senha. E tem como passar o usuário
que quero recuperar a senha (mas geralmente, ele pergunta.)

# 02 - Chamando o console CLI do RAILs - Reset de Senha - Modo 02 de Resetar a Senha / Criar uma nova

- Inicialmente, para chamar o cli RAILD, precisamos colocar no bash:

```bash
gitlab-rails console
```

Dentro do console, precisamos fazer:

```bash
# Achando o usuário de interesse.
user = User.find(<idUser>)

# Setando as variáveis
user.password = '<new_password>'
user.password_confirmation = '<repeat_new_password>'

# Realizando a consistência.
user.save!

# Parando a reconfirmação de usuário.
user.skip_reconfirmation!
```

# 03 - Reconfigurando URL Externa.

Só é preciso alterar a opção: `external_url` no arquivo do `gitlab.rb`.

# 04 - Git Config - Cheatsheet

## Conteúdo

### Git Config

- Configurando o nome do usuário (global): `git config --global user.name "Nome do Usuário"`
- Configurando email do usuário (global): `git config --global user.email "Nome do Usuário"`
- Configurando o editor de texto padrão que irá fazer os textos do commit (system): `git config --system core.editor <Nome do Editor>`
- Verificando/editando as configurações globais do git: `git config --global --edit` ou `vim $HOME/.gitconfig`
- Realizando alias em comandos do Git: `git config --global alias.<nome_atalho> "comando que irá ser substituido"`
- Listando tudo o que temos configurado: `git config --list` ou `git config -l (--global || --system || --local)`
- Colocando "corzinhas" no git: `git config --system color.ui true`

#### Git Config - Contextos

- global: configurações globais para o usuário atual (`$HOME/.gitconfig`).
- system: configurações para todo o sistema (`/etc/gitconfig`).
- local: configurações para o proprio repositório (`$MEU_REPO/.git/config`).

# 05 - Primeiros passos com o Git. Git Commands Basics.

### CLI

- Clonando um repositório: `git clone "URL/SSH do Repositorio"`
- Verificando o status de um repositório (se tem algum arquivo não trackeado, ou não commitado): `git status`
- Adicionando um arquivo que acabou de ser criado no repositorio (add no INDEX): `git add "nome do arquivo"`, ou, para adicionar todo mundo: `git add -A`, ou: `git add .` para adicionar no mesmo diretorio.
- Criando um commit ("afirmando as ações nos arquivos, add os mesmos no HEAD"): `git commit -am "Mensagem"`.
- Sincronizando o repositório com o arquivos remotos: `git push origin BRANCH`.
- Listando os logs do repositório: `git log`.
- Listando o ultimos N logs do repositório: `git log -N`.
- Listando logs do repositório em uma linha: `git log --oneline`.
- Listando logs do repositório, mostrando as diferenças que ocorreram em cada commit (detalhe do detalhe): `git log -p`.
- Listando logs do repositório com algumas informações "bonitas": `git log --pretty=<OPÇÕES>`
- Listando logs do repositório mostrando os arquivos alterados (com a quantidade de linhas add e removidas): `git log --stat`
- Listando logs do repositório mostrando as branchs: `git log --graph --decorate`
- Listando logs do repositório que contenha o nome de um arquivo editado: `git log -- <STRING PESQUISA>`
- Listando logs do repositório por author: `git log --author=<NOME AUTOR>`

### Status de um repositorio local do Git.

- Working Dir: local do repositorio local, onde o git não possui controle sobre o arquivo que está sendo trabalhado.
- Index: local do repositorio local, onde o git consegue já rastrear o arquivo que está sendo editado (`git add`). Mas o arquivo ainda segue sem está versionado.
- HEAD: local do repositorio, onde o arquivo já foi criado, e já foi versionado (após fazer um `git commit`).
- Remote: local remoto onde está o repositório na nuvem.

# 06 - O Git Branch | Merge | Revert | Reset

## Fazendo o ReMain

```bash
git branch -m master main # Copiando todo o historico da branch master para main
git push -u origin main # Dando o push no repositorio remoto, já criando a branch main.
git symbolic-ref refs/remotes/origin/HEAD refs/remotes/origin/main # Alterando o HEAD, para pegar o main como branch principal.
```

O resto do processo para fazer o ReMain, é feito na interface do usuário. Só é necessário mudar a branch default e excluir a branch antiga.

## CLI - Comandos

- Criando uma nova branch: `git checkout -b <nome da branch>`
- Listando as branchs existentes localmente: `git branch`
- Listando as branchs existentes no remoto: `git branch -a`
- Mudando de Branch: `git checkout <nome da branch>`
- Fazendo um merge (na branch que irá permanecer): `git merge <nome da branch que possui os updates>`
- Excluíndo uma branch: `git branch -d <nome da branch>`
- Excluíndo uma branch no repositório remoto: `git push -d origin <nome branch>`
- Retirando um arquivo da staging área (mas permanecendo as alterações): `git reset <nome_arquivo>`.

##### Aula de Git Reset | Revert

- Retirando as alterações do último commit com perca de alteções (**SEMPRE COM CUIDADO!!**): `git reset --hard`
- Alterando a mensagem do último commit realizado: `git commit --amend`
- Revertendo o projeto para um estado anterior (commit anterior): `git revert <id_commit>`. **OBS:** Esse comando é perigoso no sentido de que todo o trabalho anterior será perdido no revert.
- Apontando quem está fora da staging área: `git clean -n`.
- Removendo tudo o que não pertence ao atual repositório (está no _working directory_ área): `git clean -f`.
- Revertendo as alterações locais, mas mantendo os arquivos na 'untracked' área: `git reset`.
- Revertendo as alterações para um commit anterior (sem remover nada que foi criado): `git reset <commit>`.

##### Aula de Diff

> Aparentemente, o GIT DIFF só funciona para os arquivos dentro da staging área, ou seja, arquivos já trackeados.

- Apontando as diferenças entre os arquivos já trackeados do git no working directory com o último commit (o HEAD): `git diff HEAD`.
- Apontando as diferenças entre o último commit com o que está na staging área: `git diff --cached`.
- Diferença entre dois commits: `git diff <commit_mais_antigo> <commit_mais_novo>`.
- Diferença entre duas branchs: `git diff <branch_01> <branch_02>`. O diff só vai funcionar com arquivos já commitados.
