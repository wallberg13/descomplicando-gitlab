# Day 01 - Instalação da propria instancia do GitLab.

- Site da documentação: https://docs.gitlab.com/ce/install/requirements.html
- Site da instalação para Ubuntu: https://about.gitlab.com/install/#ubuntu
- Site Next Steps: https://docs.gitlab.com/ee/install/next_steps.html

### GitLab - Files

- `/etc/gitlab/initial_root_password`: arquivo com a senha inicial do gitlab,
- `/etc/gitlab/gitlab.rb`: arquivo de configuração do gitlab. Todas as configs são feitas nele.
- `/vat/opt/gitlab`: pasta com arquivos de configuração de cada ferramenta utiliada pelo gitlab.

### GitLab - gitlab.rb

- `external_url`: para configurar a URL de acesso do Gitlab local.
- `gitlab_rails['time_zone']`: para configurar o timezone do servidor.
- `gitlab_rails['gitlab_default_theme']`: tema padrão.

### CLI

- `gitlab-ctl reconfigure`: reconfigurando todo o gitlab (comando demorado). Rodar todas
  as vezes que o arquivo de configuração for atualizado (`/etc/gitlab/gitlab.rb`).

# Resumo do primeiro dia:

- Instalar a instancia do GitLab, colocar alguns arquivos de configuração, criar um repositorio local na instancia que foi criada, adicionar uma chave SSH e fazer um commit.
