# Day 03 - Primeiros Passos no GitLab.com

# 01 - Primeiros Passos

- Criar uma organização.
- Pegar o periodo trial de 30 dias do GitLab (só é bom pegar se tivermos a garantia que que vamos
  fazer todo o curso até lá).
- O Ultimate Trial mostra uns boards Kanbam.

### CLI - Criando repositório direto via CLI.

- Para iniciar um projeto Git já definindo a branch principal: `git init --initial-branch=main`.
- Adicionando uma URL para um repositório remoto: `git remote add origin <url_repo_remote>`.
- Verificando a URL do repositório remoto: `git remove -v`
- Removendo o origin do repositório: `git remote remove <nome do remote>`

## Resolvendo o Troubleshooting no Git - Verificando as CLI de SSH (selecionando a chave que vamos utilizar)

- Para saber se está ok no ssh com o Git: `ssh -T git@gitlab.com`. O servidor Git vai entregar uma resposta de boas vindas com o login do usuário.

- Criando um config no SSH (em `~/.ssh/config`):

```bash
Host gitlab.com
User git
Port 22
Hostname gitlab.com
IdentityFile ~/.ssh/<path da nova chave>
TCPKeepAlive yes
IdentitiesOnly yes
```

Então básicamente, isso só aconteceu, devido o PC do Jefferson do curso ter mais de uma chave, e ele estava usando a chave errada para subir os updates no repositório.
Serve de alerta para quando eu tiver algum problema com chaves SSH.

# Issues

Issues é um lugar onde podemos adicionar tarefas ao projeto. Ou novos objetivos para o mesmo. Tais issues pode resultar em MergeRequests,
ou em novos commits. Issues também podem ser relacionados a possíveis problemas no projeto.

Geralmente, uma Issue é resolvida com um _Merge Request_, que é o que faz a mesma ser considerada resolvida, pois o tópico criado sempre é resolvido
através de código.

Issues podem ter responsáveis, data de conclusão, finalização, tempo decorrido, objetivos entre outras infos.

Então, basicamente o processo é o seguinte:

- Cria a ISSUE. Dentro da issue, criamos um _Merge Request_ (ele cria uma _branch_ automático). Se a gente já tivermos um código
  para resolver a issue, então a gente faz um _merge_ entre a _branch_ que já estavamos trabalhando para a _branch_ que vamos subir o nosso
  código. Ai depois disso, é subir o código e partir para o abraço.

# Labels

Label é uma maneira de categorizar os _MergeRequests_ e _Issues_, já que nem toda _issue_ é um problema, as vezes pode ser uma melhoria, ou
alguma discussão que pode envolver também uma melhoria.

Para criar novas _labels_, só precisamos ir em "Project Information / Labels", dentro do GitLab. E sair criando as _labels_.

# Tags

Tags são marcações de _commits_ que são feitas quando queremos que aquele _commit_ possua um significado maior dentro de um projeto,
como se o _commit_ fosse marcar uma _release_ do projeto ou algo do tipo.

### CLI - Commands

- Criando uma tag: `git tag -a <nome_tag> -m <mensagem para a tag>`
- Listando as tags existentes: `git tag -l`
- Mostrando os detalhes do commit que foi realizado o vinculo com a tag: `git show <nome_tag>`
- Para criar tags posteriormente: `git tag -a <nome_tag> <commit>`

# O .gitignore

O _.gitignore_ é um arquivo no qual, deixamos registrado nele, todos os arquivos que não queremos que vá para o
repositório web, como o nosso arquivo de senhas.

Podemos, dentro do _.gitignore_, pedir para ele não incluir um arquivo em especifico (que esteja dentro das regras descritas
anteriormente para ignorar), bastando colocar um `!` na frente da entrada de nome.
